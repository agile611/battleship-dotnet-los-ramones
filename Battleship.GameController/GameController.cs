﻿using System.Linq;
using System.Windows.Media;

namespace Battleship.GameController
{
    using System;
    using System.Collections.Generic;

    using Battleship.GameController.Contracts;

    /// <summary>
    ///     The game controller.
    /// </summary>
    public class GameController
    {
        /// <summary>
        /// Checks the is hit.
        /// </summary>
        /// <param name="ships">
        /// The ships.
        /// </param>
        /// <param name="shot">
        /// The shot.
        /// </param>
        /// <returns>
        /// True if hit, else false
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// ships
        ///     or
        ///     shot
        /// </exception>
        public static ShipSunk CheckIsHit(IEnumerable<Ship> ships, Position shot)
        {
            ShipSunk resultHit = new ShipSunk();
            if (ships == null)
            {
                throw new ArgumentNullException("ships");
            }

            if (shot == null)
            {
                throw new ArgumentNullException("shot");
            }

            foreach (var ship in ships)
            {
                foreach (var position in ship.Positions)
                {
                    if (position.Equals(shot))
                    {
                        position.Hit = true;
                        resultHit.isHit = true;
                        resultHit.ship = ship;
                        return resultHit;
                    }
                }
            }
            resultHit.isHit = false;
            return resultHit;
        }

        public static int numBarcosSinHundir(IEnumerable<Ship> ships)
        {
            return ships.Count(barco => barco.IsHundido() == false);
        }
        /// <summary>
        ///     The initialize ships.
        /// </summary>
        /// <returns>
        ///     The <see cref="IEnumerable" />.
        /// </returns>
        public static IEnumerable<Ship> InitializeShips()
        {
            return new List<Ship>()
                       {
                           new Ship() { Name = "Aircraft Carrier", Size = 5, Color = Colors.CadetBlue }, 
                           new Ship() { Name = "Battleship", Size = 4, Color = Colors.Red }, 
                           new Ship() { Name = "Submarine", Size = 3, Color = Colors.Chartreuse }, 
                           new Ship() { Name = "Destroyer", Size = 3, Color = Colors.Yellow }, 
                           new Ship() { Name = "Patrol Boat", Size = 2, Color = Colors.Orange }
                       };
        }

        /// <summary>
        /// The is ships valid.
        /// </summary>
        /// <param name="ship">
        /// The ship.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool IsShipValid(Ship ship)
        {
            return ship.Positions.Count == ship.Size;
        }

        public static Position GetRandomPosition()
        {
            var random = new Random();
            var letter = (Letters)random.Next(6);
            var number = random.Next(1, 8);
            var position = new Position(letter, number);
            return position;
        }

        public static Position PositionValid(Position posOther, Ship myShip, List<Ship> allShips, int PosActual, DateTime dtCompare, bool IsRandom)
        {
            string input;
            Letters letter;
            int number = 0;

            while (!(allShips.Where(s => s.PositionInUse(posOther)).Count() == 0))
            {
                if (!IsRandom)
                {
                    Console.WriteLine("This position is in use, enter other position {0} of {1} (i.e A3):", PosActual, myShip.Size);
                    input = Console.ReadLine().ToUpper();
                    letter = (Letters)Enum.Parse(typeof(Letters), input.ToUpper().Substring(0, 1));
                    number = int.Parse(input.Substring(1, 1));
                    posOther = new Position(letter, number);
                }
                else
                {
                    posOther = GetRandomPosition();
                }
            }

            while (!myShip.PositionIsOk(posOther))
            {
                if (!IsRandom)
                {
                    Console.WriteLine("This position not valid, enter other position {0} of {1} (i.e A3):", PosActual, myShip.Size);
                    input = Console.ReadLine().ToUpper();
                    letter = (Letters)Enum.Parse(typeof(Letters), input.ToUpper().Substring(0, 1));
                    number = int.Parse(input.Substring(1, 1));
                    posOther = new Position(letter, number);
                }
                else
                {
                    if (dtCompare < DateTime.Now)
                    {
                        return null;
                    }

                    posOther = GetRandomPosition();
                }

                while (!(allShips.Where(s => s.PositionInUse(posOther)).Count() == 0))
                {
                    if (!IsRandom)
                    {
                        Console.WriteLine("This position is in use, enter other position {0} of {1} (i.e A3):", PosActual, myShip.Size);
                        input = Console.ReadLine().ToUpper();
                        letter = (Letters)Enum.Parse(typeof(Letters), input.ToUpper().Substring(0, 1));
                        number = int.Parse(input.Substring(1, 1));
                        posOther = new Position(letter, number);
                    }
                    else
                    {
                        posOther = GetRandomPosition();
                    }
                }
            }

            return posOther;
        }

        public static void RandomPosition(List<Ship> allShips)
        {
            List<Position> pos = new List<Position>();
            DateTime dtEntrada;

            foreach(Ship barco in allShips)
            {
                dtEntrada = DateTime.Now.AddSeconds(10);

                for (int i = 0; i < barco.Size; i++)
                {
                    Position p = GetRandomPosition();
                    if (dtEntrada < DateTime.Now)
                    {
                        barco.Positions = new List<Position>();
                        dtEntrada = DateTime.Now.AddSeconds(10);
                        i = 0;
                    }

                    Position newP = GameController.PositionValid(p, barco, allShips, i, dtEntrada, true);
                    if (newP == null)
                    {
                        barco.Positions = new List<Position>();
                        dtEntrada = DateTime.Now.AddSeconds(10);
                        i = 0;                        
                    }
                    else
                    {
                        barco.AddPosition(newP);
                    }                    
                }
            }
        }
        //Hola
    }

    public struct ShipSunk
    {
        public bool isHit;
        public Ship ship;
    }
}