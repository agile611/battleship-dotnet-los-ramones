﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Media;
using Battleship.GameController.Annotations;
using System.Linq;

namespace Battleship.GameController.Contracts
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The ship.
    /// </summary>
    public class Ship: INotifyPropertyChanged
    {
        private bool isPlaced;
        private bool isVertical;

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Ship"/> class.
        /// </summary>
        public Ship()
        {
            Positions = new List<Position>();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the positions.
        /// </summary>
        public List<Position> Positions { get; set; }

        /// <summary>
        /// The color of the ship
        /// </summary>
        public Color Color { get; set; }

        /// <summary>
        /// Gets or sets the size.
        /// </summary>
        public int Size { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add position.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        public void AddPosition(string input)
        {
            if (Positions == null)
            {
                Positions = new List<Position>();
            }

            var letter = (Letters)Enum.Parse(typeof(Letters), input.ToUpper().Substring(0, 1));
            var number = int.Parse(input.Substring(1, 1));
            Positions.Add(new Position { Column = letter, Row = number });
        }

        public void AddPosition(Position pos)
        {
            if (Positions == null)
            {
                Positions = new List<Position>();
            }

            Positions.Add(pos);
        }

        public bool IsPlaced
        {
            get { return isPlaced; }
            set
            {
                if (value.Equals(isPlaced)) return;
                isPlaced = value;
                OnPropertyChanged();
            }
        }

        public bool PositionInUse(Position posOther)
        {
            foreach (Position p in Positions)
            {
                if (p.Column == posOther.Column && p.Row == posOther.Row)
                {
                    return true;
                }
            }

            return false;
        }

        public bool PositionIsOk(Position posOther)
        {
            int numAnt = Positions.Count;

            if (numAnt == 0)
            {
                return true;
            }
            else if (numAnt > 0)
            {
                Position lastPosition = Positions[Positions.Count - 1];
                Position firstPosition = Positions[0];

                if (CompruebaPosicion(posOther, lastPosition, numAnt) || CompruebaPosicion(posOther, firstPosition, numAnt))
                {
                    return true;
                }                
            }

            return false;
        }

        private bool CompruebaPosicion(Position posOther, Position pos, int numAnt)
        {
            if (numAnt == 1)
            {
                /*Comprobamos que si es vertical sea la misma columna. si cumple, devuelve true*/
                if (pos.Row == posOther.Row + 1 || pos.Row == posOther.Row - 1)
                {
                    if (pos.Column == posOther.Column)
                    {
                        this.isVertical = true;
                        return true;
                    }
                }
                else if (pos.Row == posOther.Row) /*Seria vertical*/
                {
                    if ((int)posOther.Column == (int)pos.Column - 1 || (int)posOther.Column == (int)pos.Column + 1)
                    {
                        this.isVertical = false;
                        return true;
                    }
                }
            }
            else /*Aquí ya sabemos si es vertical u horizontal*/
            {
                if (this.isVertical)
                {
                    if (pos.Row == posOther.Row + 1 || pos.Row == posOther.Row - 1)
                    {
                        if (pos.Column == posOther.Column)
                        {
                            return true;
                        }
                    }
                }
                else if (pos.Row == posOther.Row) /*Seria vertical*/
                {
                    if ((int)posOther.Column == (int)pos.Column - 1 || (int)posOther.Column == (int)pos.Column + 1)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool IsHundido()
        {
            int posicionesTocadas = Positions.Count(p => p.Hit == true);
            return Positions.Count == posicionesTocadas;
        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }        
    }
}