﻿
namespace Battleship.Ascii
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using Battleship.GameController;
    using Battleship.GameController.Contracts;

    internal class Program
    {
        private static List<Ship> myFleet;
        private static List<Position> humanPositions = new List<Position>();

        private static List<Ship> enemyFleet;
        private static List<Position> computerPositions = new List<Position>();
        private static int numRondas = 0;



        static void Main()
        {
            PintarPantallaInicio();
            bool seguirJugando = true;
            do
            {
                InitializeGame();

                StartGame();

                seguirJugando = SeguirJugando();
            }
            while (seguirJugando);
        }

        private static void StartGame()
        {
            int NumeroTirada = 0;

            Console.Clear();
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("                  __");
            Console.WriteLine(@"                 /  \");
            Console.WriteLine("           .-.  |    |");
            Console.WriteLine(@"   *    _.-'  \  \__/");
            Console.WriteLine(@"    \.-'       \");
            Console.WriteLine("   /          _/");
            Console.WriteLine(@"  |      _  /""");
            Console.WriteLine(@"  |     /_\'");
            Console.WriteLine(@"   \    \_/");
            Console.WriteLine(@"    """"""""");

            do
            {                
                Console.WriteLine();
                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("HUMAN PLAYER");
                Console.WriteLine("- - - - - - - ");

                Console.WriteLine("Player, it's your turn");
                Console.WriteLine("Enter coordinates for your shot :");

                Position position = null;

                while ((position = ParsePosition(Console.ReadLine())) == null)
                {
                    Console.WriteLine("Miss! Posición fuera de tablero! Repeat coordiantes for your shot :");
                }

                NumeroTirada++;
                Console.Title = "Ramonships: Turno " + NumeroTirada;


                // Añadir posicion que disparamos
                humanPositions.Add(position);

                ShipSunk resultHit = GameController.CheckIsHit(enemyFleet, position);
                var isHit = resultHit.isHit;
                if (isHit)
                {
                    Console.Beep();


                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Boom();
                    Console.BackgroundColor = ConsoleColor.Cyan;
                    Console.ForegroundColor = ConsoleColor.DarkBlue;
                    Console.WriteLine("                                        Yeah ! Nice hit !");
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    if (resultHit.ship.IsHundido())
                    {
                        PintaBarco(resultHit.ship);
                        Console.WriteLine(@"        BARCO HUNDIDO: " + resultHit.ship.Name);
                        Console.WriteLine(@"        QUEDAN  " + GameController.numBarcosSinHundir(enemyFleet) + " BARCOS POR HUNDIR");
                    }
                }

                else
                {
                    Console.BackgroundColor = ConsoleColor.Blue;
                    Console.WriteLine("                                        Miss: Glu Glu!!");
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                }

                Thread.Sleep(3000);

                Console.BackgroundColor = ConsoleColor.Black;

                // Pintado de tableros
                PintarTableros();


                // Reseteamos a texto normal
                

                // Guardar posiciones disparo computer
                position = GetRandomPosition();

                while (computerPositions.Contains(position))
                    position = GetRandomPosition();

                computerPositions.Add(position);

                // Comprobar disparo
                resultHit = GameController.CheckIsHit(myFleet, position);
                isHit = resultHit.isHit;
                // Delay para separar jugadores
                Thread.Sleep(2000);
                Console.Clear();

                Console.WriteLine();
                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("COMPUTER PLAYER");
                Console.WriteLine("- - - - - - - ");

                Console.WriteLine("Computer shot in {0}{1} and {2}", position.Column, position.Row, isHit ? "has hit your ship !" : "miss");
                if (isHit)
                {
                    Console.Beep();

                    Boom();

                    if (resultHit.ship.IsHundido())
                    {
                        PintaBarco(resultHit.ship);
                        Console.WriteLine(@"        BARCO HUNDIDO: " + resultHit.ship.Name);
                        Console.WriteLine(@"        QUEDAN  " + GameController.numBarcosSinHundir(myFleet) + " BARCOS POR HUNDIR");
                    }
                }
                // HundirTodos();
                ModeReview();
            }
            while (HaTerminadoElJuego());
        }

        internal static Position ParsePosition(string input)
        {            
            // Validar argumentos
            if (input.Length != 2)
                return null;

            string letra = input.ToUpper().Substring(0, 1);
            if (letra[0] < 'A' || letra[0] > 'H')
                return null;
            
            var letter = (Letters)Enum.Parse(typeof(Letters), letra);
            var number = int.Parse(input.Substring(1, 1));
            if (number < 1 || number > 8)
                return null;

            return new Position(letter, number);
        }

        private static Position GetRandomPosition()
        {
            var random = new Random();
            var letter = (Letters)random.Next(6);
            var number = random.Next(1, 8);
            var position = new Position(letter, number);
            return position;
        }

        private static void InitializeGame()
        {
            numRondas = 0;
            InitializeMyFleet();

            InitializeEnemyFleet();

            humanPositions = new List<Position>();
            computerPositions = new List<Position>();

            PintarTableros();
        }

        private static void InitializeMyFleet()
        {
            myFleet = GameController.InitializeShips().ToList();


            Console.WriteLine("**************************************************************************");
            Console.WriteLine("***************       I N S T R U C T I O N S      ***********************");
            Console.WriteLine("**************************************************************************");

            Console.WriteLine("Introduzca 'S' si desea que le cree los barcos de modo aleatorio");
            if (Console.ReadLine().ToUpper().Equals("S"))
            {
                Console.WriteLine("La IA está preparando tu armada!!! Así que espera unos segundos...");
                GameController.RandomPosition(myFleet);
            }
            else
            {
                Console.WriteLine("Please position your fleet (Game board size is from A to H and 1 to 8) :");

                foreach (var ship in myFleet)
                {
                    PintaBarco(ship);
                    Console.WriteLine();
                    Console.WriteLine("Please enter the positions for the {0} (size: {1})", ship.Name, ship.Size);
                    for (var i = 1; i <= ship.Size; i++)
                    {
                        Console.WriteLine("Enter position {0} of {1} (i.e A3):", i, ship.Size);

                        var position = Console.ReadLine().ToUpper();
                        try
                        {
                            Letters letter = (Letters)Enum.Parse(typeof(Letters), position.ToUpper().Substring(0, 1));
                            int number = int.Parse(position.Substring(1, position.Length-1));                           
                            Position posOther = new Position(letter, number);
                            if (number < 1 || number > 8)
                            {
                                Console.WriteLine("****************************");
                                Console.WriteLine("Error. Incorrect position");
                                i--;
                            }
                            else
                            {
                                ship.AddPosition(GameController.PositionValid(posOther, ship, myFleet, i, DateTime.Now.AddDays(1), false));
                            }
                        }
                        catch (Exception ex)
                        {

                            Console.WriteLine("****************************");
                            Console.WriteLine("Error. Incorrect position");
                            i--;
                        }
                    }
                }
            }
        }

        private static void InitializeEnemyFleet()
        {
            enemyFleet = GameController.InitializeShips().ToList();
            Console.WriteLine("La IA está preparando la armada del ordenador!!! Así que espera unos segundos...");
            GameController.RandomPosition(enemyFleet);

        }

        private static bool HaTerminadoElJuego()
        {

            if (TodosBarcosHundidos(enemyFleet))
            {
                Winner();
                FinJuego("JUGADOR 1");
                return false;
            }
            if (TodosBarcosHundidos(myFleet))
            {
                GameOver();
                FinJuego("COMPUTER");
                return false;
            }
            numRondas++;
            return true;
        }
        private static bool TodosBarcosHundidos(List<Ship> barcos)
        {
            return GameController.numBarcosSinHundir(barcos) == 0;
        }

        private static void FinJuego(string Jugador)
        {
            Console.WriteLine(@" **************  " + Jugador + " WIN  **************");
        }

        private static void HundirFlota(List<Ship> fleet)
        {
            foreach (Ship barco in fleet)
            {
                foreach (Position pos in barco.Positions)
                {
                    pos.Hit = true;
                }
            }
        }

        private static bool SeguirJugando()
        {
            do
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("Quiere seguir jugando (Si/No):");
                string respuesta = Console.ReadLine().ToUpper();
                if (respuesta == "SI")
                {
                    PintarPantallaInicio();
                    return true;
                }
                else if (respuesta == "NO")
                {
                    return false;
                }
                Console.WriteLine("Respuesta no valida, introduzca Si o No.");
            }
            while (true);

        }

        #region interfaz grafica
        //interfaz grafica
        private static void PintarPantallaInicio()
        {
            Console.Title = "Bienvenido a Ramonship";
            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.ForegroundColor = ConsoleColor.White;
            Console.Clear();
            
            Console.WriteLine("                                     |__");
            Console.WriteLine(@"                                     |\/");
            Console.WriteLine("                                     ---");
            Console.WriteLine("                                     / | [");
            Console.WriteLine("                              !      | |||");
            Console.WriteLine("                            _/|     _/|-++'");
            Console.WriteLine("                        +  +--|    |--|--|_ |-");
            Console.WriteLine(@"                     { /|__|  |/\__|  |--- |||__/");
            Console.WriteLine(@"                    +---------------___[}-_===_.'____                 /\");
            Console.WriteLine(@"                ____`-' ||___-{]_| _[}-  |     |_[___\==--            \/   _");
            Console.WriteLine(@" __..._____--==/___]_|__|_____________________________[___\==--____,------' .7");
            Console.WriteLine(@"|                        Welcome to Battleship                         BB-61/");
            Console.WriteLine(@" \_________________________________________________________________________|");
            Console.WriteLine();
        }
        private static void PintaBarco(Ship barco)
        {
            switch (barco.Size)
            {
                case 5:
                    PintaBarco5(barco.Name);
                    break;
                case 4:
                    PintaBarco4(barco.Name);
                    break;
                case 3:
                    PintaBarco3(barco.Name);
                    break;
                case 2:
                    PintaBarco2(barco.Name);
                    break;
                default:
                    break;
            }
        }

        private static void PintaBarco2(string nombreBarco)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("  " + nombreBarco + "");
            Console.WriteLine("       _~");
            Console.WriteLine("    _~ )_)");
            Console.WriteLine("    )_))_)");
            Console.WriteLine("    _!__!___");
            Console.WriteLine("    \\_____t/");
            Console.WriteLine("  ~~~~~~~~~~~~~");
            Console.ForegroundColor = ConsoleColor.White;
        }
        private static void PintaBarco3(string nombreBarco)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("  " + nombreBarco + "");
            Console.WriteLine("       _~ ");
            Console.WriteLine("    _~ )_)_~");
            Console.WriteLine("    )_))_))_)");
            Console.WriteLine("    _!__!__!_");
            Console.WriteLine("    \\______t/");
            Console.WriteLine("  ~~~~~~~~~~~~~");
            Console.ForegroundColor = ConsoleColor.White;
        }
        private static void PintaBarco4(string nombreBarco)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("  " + nombreBarco + "");
            Console.WriteLine("       _~    _~");
            Console.WriteLine("    _~ )_)_~ )_)");
            Console.WriteLine("    )_))_))_))_)");
            Console.WriteLine("    _!__!__!__!_");
            Console.WriteLine("    \\_________t/");
            Console.WriteLine("  ~~~~~~~~~~~~~~~~");
            Console.ForegroundColor = ConsoleColor.White;

        }
        private static void PintaBarco5(string nombreBarco)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("  " + nombreBarco + "  ");
            Console.WriteLine("       _~    _~");
            Console.WriteLine("    _~ )_)_~ )_)_~");
            Console.WriteLine("    )_))_))_))_))_)");
            Console.WriteLine("    _!__!__!__!__!_");
            Console.WriteLine("    \\____________t/");
            Console.WriteLine("  ~~~~~~~~~~~~~~~~~~~");
            Console.ForegroundColor = ConsoleColor.White;
        }

        private static void GameOver()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼");
            Console.WriteLine("███▀▀▀██┼███▀▀▀███┼███▀█▄█▀███┼██▀▀▀");
            Console.WriteLine("██┼┼┼┼██┼██┼┼┼┼┼██┼██┼┼┼█┼┼┼██┼██┼┼┼");
            Console.WriteLine("██┼┼┼▄▄▄┼██▄▄▄▄▄██┼██┼┼┼▀┼┼┼██┼██▀▀▀");
            Console.WriteLine("██┼┼┼┼██┼██┼┼┼┼┼██┼██┼┼┼┼┼┼┼██┼██┼┼┼");
            Console.WriteLine("███▄▄▄██┼██┼┼┼┼┼██┼██┼┼┼┼┼┼┼██┼██▄▄▄");
            Console.WriteLine("┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼");
            Console.WriteLine("███▀▀▀███┼▀███┼┼██▀┼██▀▀▀┼██▀▀▀▀██▄┼");
            Console.WriteLine("██┼┼┼┼┼██┼┼┼██┼┼██┼┼██┼┼┼┼██┼┼┼┼┼██┼");
            Console.WriteLine("██┼┼┼┼┼██┼┼┼██┼┼██┼┼██▀▀▀┼██▄▄▄▄▄▀▀┼");
            Console.WriteLine("██┼┼┼┼┼██┼┼┼██┼┼█▀┼┼██┼┼┼┼██┼┼┼┼┼██┼");
            Console.WriteLine("███▄▄▄███┼┼┼─▀█▀┼┼─┼██▄▄▄┼██┼┼┼┼┼██▄");
            Console.WriteLine("┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼");
            Console.WriteLine("┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼█┼┼┼┼┼┼┼┼┼┼┼┼┼┼");
            Console.WriteLine("┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼███┼┼┼┼┼┼┼┼┼┼┼┼┼");
            Console.WriteLine("┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼████┼┼┼┼┼┼┼┼┼┼┼┼");
            Console.WriteLine("┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼██████┼┼┼┼┼┼┼┼┼┼┼");
            Console.WriteLine("┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼████████┼┼┼┼┼┼┼┼┼┼");
            Console.WriteLine("┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼██████████┼┼┼┼┼┼┼┼┼");
            Console.WriteLine("┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼████████████┼┼┼┼┼┼┼┼");
            Console.WriteLine("┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼██████████████┼┼┼┼┼┼┼");
            Console.WriteLine("┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼█████████████┼┼┼┼┼┼┼");
            Console.WriteLine("┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼████████████┼┼┼┼┼┼┼");
            Console.WriteLine("┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼█████████┼┼┼┼┼┼┼┼");
            Console.WriteLine("┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼");
            Console.WriteLine("┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼");
            Console.WriteLine("┼┼┼┼┼┼┼┼┼┼█┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼");
            Console.WriteLine("┼┼┼┼┼┼┼┼┼███┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼");
            Console.WriteLine("┼┼┼┼┼┼┼┼┼████┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼");
            Console.WriteLine("┼┼┼┼┼┼┼┼██████┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼");
            Console.WriteLine("┼┼┼┼┼┼┼████████┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼");
            Console.WriteLine("┼┼┼┼┼┼██████████┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼");
            Console.WriteLine("┼┼┼┼┼████████████┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼");
            Console.WriteLine("┼┼┼┼██████████████┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼");
            Console.WriteLine("┼┼┼┼┼█████████████┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼");
            Console.WriteLine("┼┼┼┼┼┼████████████┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼");
            Console.WriteLine("┼┼┼┼┼┼┼┼█████████┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼");
            Console.WriteLine("┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼");
        }
        private static void Boom()
        {
            //Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("------------------------------------------------------------------");
            Console.WriteLine(@"                    \         .  ./");
            Console.WriteLine(@"                  \      .:"";'.:..""   /");
            Console.WriteLine(@"                      (M^^.^~~:.'"").");
            Console.WriteLine(@"                -   (/  .    . . \ \)  -");
            Console.WriteLine(@"                   ((| :. ~ ^  :. .|))");
            Console.WriteLine(@"                -   (\- |  \ /  |  /)  -");
            Console.WriteLine(@"                     -\  \     /  /-");
            Console.WriteLine(@"                       \  \   /  /");
            Console.WriteLine("------------------------------------------------------------------"); 
        }
        private static void Winner()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(" __________________________________________________________");         
            Console.WriteLine("__________________________________________________________");
            Console.WriteLine("________¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶");
            Console.WriteLine("________¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶");
            Console.WriteLine("___¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶");
            Console.WriteLine("_¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶");
            Console.WriteLine("¶¶¶¶______¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶_______¶¶¶¶");
            Console.WriteLine("¶¶¶_______¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶________¶¶¶");
            Console.WriteLine("¶¶________¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶________¶¶¶");
            Console.WriteLine("¶¶¶_____¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶______¶¶¶");
            Console.WriteLine("¶¶¶____¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶____¶¶¶¶");
            Console.WriteLine("_¶¶¶___¶¶¶_¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶_¶¶¶____¶¶¶");
            Console.WriteLine("_¶¶¶¶___¶¶¶_¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶_¶¶¶¶__¶¶¶¶");
            Console.WriteLine("___¶¶¶¶__¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶_¶¶¶¶¶");
            Console.WriteLine("____¶¶¶¶¶¶¶¶_¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶_¶¶¶¶¶¶¶¶¶");
            Console.WriteLine("______¶¶¶¶¶¶__¶¶¶¶¶¶¶¶¶¶¶¶¶¶___¶¶¶¶¶¶");
            Console.WriteLine("_______________¶¶¶¶¶¶¶¶¶¶¶¶");
            Console.WriteLine("_________________¶¶¶¶¶¶¶¶");
            Console.WriteLine("___________________¶¶¶¶");
            Console.WriteLine("___________________¶¶¶¶");
            Console.WriteLine("___________________¶¶¶¶");
            Console.WriteLine("___________________¶¶¶¶");
            Console.WriteLine("_______________¶¶¶¶¶¶¶¶¶¶¶¶");
            Console.WriteLine("____________¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶");
            Console.WriteLine("____________¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶");
            Console.WriteLine("____________¶¶¶____________¶¶¶");
            Console.WriteLine("____________¶¶¶____________¶¶¶");
            Console.WriteLine("____________¶¶¶____________¶¶¶");
            Console.WriteLine("____________¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶");
            Console.WriteLine("____________¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶");
            Console.WriteLine("__________¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶");
            Console.WriteLine("_________¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶");
        }
        #endregion

        private static void PintarTableros()
        {
            Console.BackgroundColor = ConsoleColor.DarkRed;
            // Mi flota
            int[,] tableroFlota = new int[8, 8];
            for (int i = 0; i < 8; i++)
            {
                // Col
                for (int j = 0; j < 8; j++)
                {
                    tableroFlota[i, j] = 0;
                }
            }

            foreach (var ship in myFleet)
            {

                foreach (Position posActualBarco in ship.Positions)
                {
                    tableroFlota[posActualBarco.Row, (int)posActualBarco.Column] = 1;
                }
            }

            Console.Clear();

            
            Console.WriteLine("                     *********************");
            Console.WriteLine("                     TU FLOTA ESTÁ LISTA!!!");
            Console.WriteLine("                     *********************");
            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("");

            Console.BackgroundColor = ConsoleColor.Gray;
            Console.ForegroundColor= ConsoleColor.Black;
            Console.WriteLine(@"{0} {1} {2} {3} {4} {5} {6} {7} {8}",
                "       ",
                " | A | ",
                " | B | ",
                " | C | ",
                " | D | ",
                " | E | ",
                " | F | ",
                " | G | ",
                " | H | ");
            Console.WriteLine(@"{0} {1} {2} {3} {4} {5} {6} {7} {8}",
                "_______",
                "_______",
                "_______",
                "_______",
                "_______",
                "_______",
                "_______",
                "_______",
                "_______");

            for (int i = 0; i < 8; i++)
            {
                Console.WriteLine(@"{0} {1} {2} {3} {4} {5} {6} {7} {8}",
                    " | " + (i + 1).ToString() + " | ",
                    tableroFlota[i, 0] == 1 ? (" | O | ") : (" |   | "),
                    tableroFlota[i, 1] == 1 ? (" | O | ") : (" |   | "),
                    tableroFlota[i, 2] == 1 ? (" | O | ") : (" |   | "),
                    tableroFlota[i, 3] == 1 ? (" | O | ") : (" |   | "),
                    tableroFlota[i, 4] == 1 ? (" | O | ") : (" |   | "),
                    tableroFlota[i, 5] == 1 ? (" | O | ") : (" |   | "),
                    tableroFlota[i, 6] == 1 ? (" | O | ") : (" |   | "),
                    tableroFlota[i, 7] == 1 ? (" | O | ") : (" |   | "));
            }

            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");

            // Mis disparos
            int[,] tableroDisparos = new int[8, 8];
            for (int i = 0; i < 8; i++)
            {
                // Col
                for (int j = 0; j < 8; j++)
                {
                    tableroDisparos[i, j] = 0;
                }
            }

            foreach (var position in humanPositions)
            {
                tableroDisparos[position.Row-1, (int)position.Column] = 1;
            }


            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("                     *********************");
            Console.WriteLine("                        TUS DISPAROS!!!");
            Console.WriteLine("                     *********************");
            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("");

            Console.BackgroundColor = ConsoleColor.Gray;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine(@"{0} {1} {2} {3} {4} {5} {6} {7} {8}",
                "       ",
                " | A | ",
                " | B | ",
                " | C | ",
                " | D | ",
                " | E | ",
                " | F | ",
                " | G | ",
                " | H | ");
            Console.WriteLine(@"{0} {1} {2} {3} {4} {5} {6} {7} {8}",
                "_______",
                "_______",
                "_______",
                "_______",
                "_______",
                "_______",
                "_______",
                "_______",
                "_______");

            for (int i = 0; i < 8; i++)
            {
                Console.WriteLine(@"{0} {1} {2} {3} {4} {5} {6} {7} {8}",
                    " | " + (i + 1).ToString() + " | ",
                    tableroDisparos[i, 0] == 1 ? (" | H | ") : (" |   | "),
                    tableroDisparos[i, 1] == 1 ? (" | H | ") : (" |   | "),
                    tableroDisparos[i, 2] == 1 ? (" | H | ") : (" |   | "),
                    tableroDisparos[i, 3] == 1 ? (" | H | ") : (" |   | "),
                    tableroDisparos[i, 4] == 1 ? (" | H | ") : (" |   | "),
                    tableroDisparos[i, 5] == 1 ? (" | H | ") : (" |   | "),
                    tableroDisparos[i, 6] == 1 ? (" | H | ") : (" |   | "),
                    tableroDisparos[i, 7] == 1 ? (" | H | ") : (" |   | "));
            }

            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("");
            Console.WriteLine("Pulse tecla para continuar.");
            Console.ReadKey();
        }

        private static void ModeReview()
        {
            if (numRondas > 4)
            {
                Console.WriteLine("******************");
                Console.WriteLine("Quiere finalizar el juego?? (Si/No)");
                string respuesta = Console.ReadLine().ToUpper();
                if (respuesta == "SI")
                {
                    HundirFlota(myFleet);
                }
                else if (respuesta == "RAMONES")
                {
                    HundirFlota(enemyFleet);
                }
                else
                {
                    numRondas = 0;
                }
            }
        }
    }
}
